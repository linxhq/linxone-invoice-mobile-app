# linxone invoice mobile app

web backend: 
+ run command npm install to install package.
+ start database mysql server. 
+ edit database config file app/config/env.js
+ run: npm start or node server.js to start backend server.

mobile app code:
+ run command npm install to install required package.
+ edit config `serverURL` in config/api.js
+ run: expo build:android or expo build:ios to build app.