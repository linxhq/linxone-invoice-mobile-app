// const tintColor = '#282e60';
const tintColor = 'rgb(25, 42, 86)';
const firstColor = '#434ea0';
const secondColor = '#d37375';

export default {
  firstColor,
  secondColor,
  tintColor,
  // bgColor: '#e5e5e5',
  bgColor: '#fff',
  bottomActiveNavBgColor: '#3348f3',
  bottomNavBgColor: '#efefef',
  tabIconDefault: '#ccc',
  // tabIconSelected: '#5365ff',
  tabIconSelected: 'rgb(6, 82, 221)',
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  
  blue1: 'rgb(6, 82, 221)',
  brick: '#fb607f',
  lemonade: '#fdb9c8',
  lemonade2: '#e1cdce',
  lightgreen1: 'rgb(16, 172, 132)',
  grey1: "#787a81",
  navy1: 'rgb(25, 42, 86)',
  pearlRiver: '#d9dddc',
  pearlRiver2: '#d1d9e2',
  pink1: 'rgb(255, 107, 107)',
  sea: '#258b57',
  tea: '#d0f0c0',
  tea2: '#c6dacf'
};
