import { SIGNOUT } from "../actions/actionTypes";

const initialState = false;

export default function(state = initialState, action) {
  switch (action.type) {
    case SIGNOUT:
      return (state = !state);

    default:
      return state;
  }
}
