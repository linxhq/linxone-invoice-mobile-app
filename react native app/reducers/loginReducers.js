import { SIGNIN, SIGNOUT } from "../actions/actionTypes";

const initialState = false;

export default function(state = initialState, action) {
  switch (action.type) {
    case SIGNIN:
      return (state = true);
    case SIGNOUT:
      return (state = false);
    default:
      return state;
  }
}
