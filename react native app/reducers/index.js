import { combineReducers } from "redux";

import loginReducers from "./loginReducers";
// import logoutReducers from "./logoutReducers";

const allReducers = combineReducers({
    signIn: loginReducers,
    // signOut: logoutReducers
});

export default allReducers;