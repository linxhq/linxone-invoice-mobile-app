import { SIGNIN, SIGNOUT } from "./actionTypes"

export const handleSignIn = () => {
    return {
        type: SIGNIN
    }
}
export const handleSignOut = () => {
    return {
        type: SIGNOUT
    }
}