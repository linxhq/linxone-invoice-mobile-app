import React from "react";
import {
  ActivityIndicator,
  Alert,
  FlatList,
  Image,
  ListView,
  Platform,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";

import apiConfig from "../config/api";
import { SearchBar, Input } from "react-native-elements";
import Colors from "../constants/Colors";
import { Constants, Font, Icon } from "expo";
import axios from "axios";
import qs from "qs";
import { getAccessToken } from "../auth";
import { addComma, currencyFormat } from "../lib/number";
import moment from "moment";
import { connect } from "react-redux";
import * as actions from "../actions/";
import { onSignOut } from "../auth";

function mapStateToProps(state) {
  return {
    handleSignOut: state.handleSignOut
  };
}

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      isLoading: true,
      payment: [],
      invoices: [],
      refreshing: false,
      waitingMoreInvoices: false,
      invoicesOffset: 0,
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
    };

    this.renderList = this.renderList.bind(this);
    this.onEndReached = this.onEndReached.bind(this);
    this.getMoreInvoices = this.getMoreInvoices.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.fetchData();
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.fetchData().then(() => {
      this.setState({ refreshing: false });
    });
  };

  alertError = err => {
    var mess;
    switch (err.response.status) {
      case 401:
        mess = err.response.data.reason;
        break;
      case 404:
        mess = err.response.data;
        break;
      case 500:
        mess = err.response.data.message;
        break;
      default:
        mess = err.response.data;
        break;
    }
    Alert.alert("Alert", mess, [{ text: "OK" }]);
  };

  fetchData = () => {
    return new Promise((resolve, reject) => {
      getAccessToken()
        .then(res => {
          axios
            .get(apiConfig.serverURL + "api/home", {
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "x-access-token": res
              }
            })
            .then(response => {
              this.setState({
                payment: response.data.payment,
                invoices: response.data.invoices,
                dataSource: this.state.dataSource.cloneWithRows(
                  response.data.invoices
                )
              });
              this.setState({ isLoading: false });
              resolve(true);
            })
            .catch(err => {
              this.alertError(err);
              // reject(true);
              if (err.response.status == 500) {
                onSignOut();
                this.props.handleSignOut();
              }
            });
        })
        .catch(err => {
          this.alertError(err);
          // reject(true);
        });
    });
  };

  getMoreInvoices() {
    return new Promise((resolve, reject) => {
      getAccessToken()
        .then(res => {
          axios
            .get(apiConfig.serverURL + "api/moreinvoices", {
              params: { offset: this.state.invoicesOffset + 10 },
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "x-access-token": res
              }
            })
            .then(response => {
              if (response.data.length != 0) {
                this.setState({
                  invoices: this.state.invoices.concat(response.data),
                  waitingMoreInvoices: false,
                  invoicesOffset: this.state.invoicesOffset + 10,
                  dataSource: this.state.dataSource.cloneWithRows(
                    this.state.invoices.concat(response.data)
                  )
                });

                resolve(true);
              } else {
                Alert.alert("Alert", "All records were loaded.", [
                  { text: "OK" }
                ]);
                resolve(false);
              }
            })
            .catch(err => {
              this.alertError(err);
              resolve(false);
            });
        })
        .catch(err => {
          this.alertError(err);
        });
    });
  }

  onEndReached() {
    if (
      !this.state.isLoading &&
      !this.state.refreshing &&
      !this.state.waitingMoreInvoices
    ) {
      this.setState({ waitingMoreInvoices: true });
      this.getMoreInvoices();
    }
  }

  renderHeader = () => {
    const { payment } = this.state;
    return (
      <View style={styles.header}>
        <View style={styles.headerTop}>
          <Text style={styles.hsHeaderTopLine1}>INCOME THIS YEAR</Text>
          <View style={styles.hsHeaderTopLine2}>
            <Text style={styles.hsHeaderTopLine2Currency}>$</Text>
            <Text style={styles.hsHeaderTopLine2Text}>
              {addComma(payment.totalThisYear)}
            </Text>
          </View>
        </View>
        <View style={styles.headerCenter}>
          <View style={styles.headerCenterSubInfo}>
            <View style={styles.hsHeaderCenterSubInfoLine1}>
              <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
              <Text style={styles.hsHeaderCenterSubInfoLine1Text}>
                {currencyFormat(payment.totalBeforeLast)}
              </Text>
            </View>
            <Text style={styles.hsHeaderCenterSubInfoLine2}>
              {moment()
                .subtract(2, "month")
                .format("MMM")
                .toUpperCase()}
            </Text>
          </View>
          <View style={styles.headerCenterSubInfo}>
            <View style={styles.hsHeaderCenterSubInfoLine1}>
              <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
              <Text style={styles.hsHeaderCenterSubInfoLine1Text}>
                {currencyFormat(payment.totalLastMonth)}
              </Text>
            </View>
            <Text style={styles.hsHeaderCenterSubInfoLine2}>
              {moment()
                .subtract(1, "month")
                .format("MMM")
                .toUpperCase()}
            </Text>
          </View>
          <View style={styles.headerCenterSubInfo}>
            <View style={styles.hsHeaderCenterSubInfoLine1}>
              <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
              <Text style={styles.hsHeaderCenterSubInfoLine1Text}>
                {currencyFormat(payment.totalLastYear)}
              </Text>
            </View>
            <Text style={styles.hsHeaderCenterSubInfoLine2}>
              {moment()
                .subtract(1, "year")
                .format("YYYY")}
            </Text>
          </View>
        </View>
        <View style={styles.headerBottom}>
          {/* <Chart > */}
          <View style={styles.headerBottomSearchbar}>
            <SearchBar
              containerStyle={styles.ssSearchbarContainer}
              inputContainerStyle={styles.ssSearchbarInputContainer}
              inputStyle={styles.ssSearchbar}
              leftIconContainerStyle={styles.ssLeftIconContainer}
              lightTheme
              onChangeText={() => {}}
              placeholder="Search by id, name, task"
              icon={{
                type: "material",
                color: "#86939e",
                name: "search",
                style: { fontSize: 30, top: 15, left: 5 }
              }}
            />
          </View>
        </View>
      </View>
    );
  };

  renderFooter = waitingMoreInvoices => {
    if (waitingMoreInvoices) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            flexDirection: "row",
            justifyContent: "space-around",
            padding: 10
          }}
        >
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
      );
    } else {
      return <Text />;
    }
  };

  renderList = (invoices, waitingMoreInvoices) => {
    if (!invoices.length) {
      return (
        <View>
          <Text>NO INVOICES TO LIST</Text>
        </View>
      );
    }
    return (
      <FlatList
        data={this.state.invoices}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
        onRefresh={this._onRefresh}
        onEndReached={this.onEndReached}
        refreshing={this.state.refreshing}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(invoice, index) => {
          return (
            <View
              style={[styles.ssBlock, styles.ssInlineBlock]}
              id={invoice.item.lb_record_primary_key}
            >
              <View
                style={[
                  styles.ssBlockIcon,
                  { backgroundColor: Colors.pearlRiver2 }
                ]}
              >
                <Icon.AntDesign
                  color={Colors.blue1}
                  name={"calendar"}
                  size={24}
                />
              </View>
              <View style={styles.ssBlockInfo}>
                <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                  <Text style={styles.ssBlockInfoLine1Text}>
                    {invoice.item.lb_customer_name || "--"}
                  </Text>
                  <View style={styles.ssBlockInfoLine1Icon}>
                    <Icon.Feather color={"#ccc"} name={"eye"} size={16} />
                  </View>
                </View>
                <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                  <Text style={styles.ssBlockInfoLine2Text1}>
                    {moment(invoice.item.lb_invoice_due_date).format("MM/DD") ||
                      "--"}
                  </Text>
                  <Text style={styles.ssBlockInfoLine2Text2}>
                    {invoice.item.lb_invoice_subject || "--"}
                  </Text>
                  <Text style={styles.ssBlockInfoLine2Text3Currency}>
                    {invoice.item.lb_invoice_total_after_taxes ? "$" : ""}
                  </Text>
                  <Text style={styles.ssBlockInfoLine2Text3}>
                    {addComma(invoice.item.lb_invoice_total_after_taxes) ||
                      "--"}
                  </Text>
                </View>
              </View>
            </View>
          );
        }}
      />
    );
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            flexDirection: "row",
            justifyContent: "space-around",
            padding: 10
          }}
        >
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
      );
    }

    const { payment, invoices } = this.state;
    return (
      <View style={styles.container}>
        {this.renderList(invoices, this.state.waitingMoreInvoices)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgColor,
    marginTop: Platform.OS === "ios" ? 0 : Constants.statusBarHeight
  },
  header: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  headerTop: {
    alignItems: "center"
  },
  hsHeaderTopLine1: {
    color: Colors.grey1,
    fontSize: 18,
    fontFamily: "Nunito-bold"
  },
  hsHeaderTopLine2: {
    flexDirection: "row",
    alignSelf: "center"
  },
  hsHeaderTopLine2Text: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    // fontSize: 50
    fontSize: 44
  },
  hsHeaderTopLine2Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 14,
    marginRight: 5
  },
  headerCenter: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20
  },
  headerCenterSubInfo: {},
  hsHeaderCenterSubInfoLine1: {
    flexDirection: "row",
    alignSelf: "center"
  },
  hsHeaderCenterSubInfoLine1Text: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 20
  },
  hsHeaderCenterSubInfoLine1Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 12,
    marginRight: 3
  },
  hsHeaderCenterSubInfoLine2: {
    alignSelf: "center",
    color: "#777",
    fontFamily: "Nunito-bold",
    fontSize: 14
  },
  headerBottom: {},
  headerBottomSearchbar: {
    marginTop: 50,
    paddingLeft: 10,
    paddingRight: 10
  },
  // main: {
  //   flex: 1,
  //   flexDirection: "row",
  //   flexWrap: "wrap",
  //   justifyContent: "space-around",
  //   marginTop: 100
  // },
  block: {
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    // marginBottom: 30,
    paddingBottom: 30
  },
  hsBlockLine1: {
    height: 40
  },
  hsBlockLine2: {
    color: Colors.grey1,
    fontFamily: "Nunito-bold",
    fontSize: 12,

    marginTop: 5,
    marginBottom: 0
  },
  hsBlockLine3: {
    color: Colors.tintColor,
    fontSize: 30,
    fontFamily: "Nunito-bold",
    marginBottom: 0
  },
  hsBlockLine3Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 12,
    marginRight: 3
  },
  hsBlock3Line3SubBlock: {
    alignSelf: "center",
    backgroundColor: Colors.brick,
    borderRadius: 3,
    color: Colors.bgColor,
    height: 22,
    fontFamily: "Nunito-bold",
    marginLeft: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  hsBlockLine4: {
    color: "grey",
    fontFamily: "Nunito",
    fontSize: 11
  },
  hsBlock4Line3SubBlock: {
    alignSelf: "center",
    backgroundColor: Colors.sea,
    borderRadius: 3,
    color: Colors.bgColor,
    height: 22,
    fontFamily: "Nunito-bold",
    marginLeft: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  hsInlineBlock: {
    flexDirection: "row",
    alignSelf: "center",
    marginBottom: 5
  },
  main: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "flex-start",
    marginTop: 40,
    paddingBottom: 0
    // paddingLeft: 20,
    // paddingRight: 20
  },
  ssBlock: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    paddingBottom: 30,
    paddingLeft: 20,
    paddingRight: 20
  },
  ssBlockIcon: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    height: 50,
    width: 50,
    marginRight: 10
  },
  ssInlineBlock: {
    flexDirection: "row"
  },
  ssSearchbarContainer: {
    backgroundColor: "transparent",
    borderTopWidth: 0,
    borderBottomWidth: 0,
    padding: 0
  },
  ssLeftIconContainer: {},
  ssSearchbarInputContainer: {},
  ssSearchbar: {
    backgroundColor: "transparent",
    fontFamily: "Nunito",
    fontSize: 16,
    paddingLeft: 30
  },
  ssSearchIcon: {
    // fontSize: 44
  },
  ssBlockInfo: {
    flexDirection: "column",
    alignItems: "flex-start"
  },
  ssBlockInfoLine1: {
    alignItems: "center"
  },
  ssBlockInfoLine1Text: {
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 20,
    marginRight: 5
  },
  ssBlockInfoLine1Icon: {
    alignSelf: "center"
  },
  ssBlockInfoLine2: {
    alignItems: "center"
  },
  ssBlockInfoLine2Text1: {
    color: Colors.tintColor,
    fontFamily: "Nunito",
    fontSize: 13,
    marginRight: 5
  },
  ssBlockInfoLine2Text2: {
    color: Colors.tintColor,
    fontFamily: "Nunito",
    fontSize: 14,
    marginRight: 5
  },
  ssBlockInfoLine2Text3Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 11
  },
  ssBlockInfoLine2Text3: {
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 15
  }
});

export default connect(
  mapStateToProps,
  actions
)(HomeScreen);
