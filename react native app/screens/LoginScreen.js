import React, { Component, PropTypes } from "react";
import {
  Alert,
  Animated,
  Dimensions,
  Easing,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View
} from "react-native";

import apiConfig from "../config/api";
import Colors from "../constants/Colors";
import { Constants, Font, Icon, LinearGradient } from "expo";
import axios from "axios";
import qs from "qs";
import { onSignIn } from "../auth";
import { connect } from "react-redux";
import * as actions from "../actions/";

var { height, width } = Dimensions.get("window");

const duration = 500;

const scale1 = 0.85;
const scale2 = 1;

const zIndex1 = 1;
const zIndex2 = 200;

function mapStateToProps(state) {
  return {
    handleSignIn: state.handleSignIn
  };
}

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  state = {
    signInEmail: "",
    signInPassword: "",
    registerEmail: "",
    registerPassword: "",
    registerRepassword: "",

    showSignInPassword: false,
    showRegisterPassword: false,
    showRegisterRepassword: false,
    signinShowing: true,
    registerShowing: false,

    beforeScale: 1,
    keyboardAvoidingViewKey: "keyboardAvoidingViewKey"
  };

  constructor(props) {
    super(props);

    this.signInPress = this.signInPress.bind(this);
    this.registerPress = this.registerPress.bind(this);

    this.signIn = this.signIn.bind(this);
  }

  componentDidMount() {
    this.keyboardHideListener = Keyboard.addListener(
      Platform.OS === "android" ? "keyboardDidHide" : "keyboardWillHide",
      this.keyboardHideListener.bind(this)
    );
  }

  componentWillUnmount() {
    this.keyboardHideListener.remove();
  }

  keyboardHideListener() {
    this.setState({
      keyboardAvoidingViewKey: "keyboardAvoidingViewKey" + new Date().getTime()
    });
  }

  componentWillMount() {
    this.signInAnimatedScale = new Animated.Value(scale2);
    this.signInAnimatedZIndex = new Animated.Value(zIndex2);

    this.registerAnimatedScale = new Animated.Value(scale1);
    this.registerAnimatedZIndex = new Animated.Value(zIndex1);
  }

  alertError = err => {
    var mess;
    switch (err.response.status) {
      case 401:
        mess = err.response.data.reason;
        break;
      case 404:
        mess = err.response.data;
        break;
      case 500:
        mess = err.response.data.message;
        break;
      default:
        mess = err.response.data;
        break;
    }
    Alert.alert("Alert", mess, [{ text: "OK" }]);
  };

  signInPress() {
    this.setState({ signinShowing: true, registerShowing: false });
    Keyboard.dismiss();
    Animated.parallel([
      Animated.timing(this.signInAnimatedScale, {
        toValue: scale2,
        easing: Easing.back(),
        duration: duration
      }),
      Animated.timing(this.signInAnimatedZIndex, {
        toValue: zIndex2,
        easing: Easing.back(),
        duration: duration
      }),

      Animated.timing(this.registerAnimatedScale, {
        toValue: scale1,
        easing: Easing.back(),
        duration: duration
      }),
      Animated.timing(this.registerAnimatedZIndex, {
        toValue: zIndex1,
        easing: Easing.back(),
        duration: duration
      })
    ]).start();
  }

  registerPress() {
    this.setState({ signinShowing: false, registerShowing: true });
    Keyboard.dismiss();
    Animated.parallel([
      Animated.timing(this.signInAnimatedScale, {
        toValue: scale1,
        easing: Easing.back(),
        duration: duration
      }),
      Animated.timing(this.signInAnimatedZIndex, {
        toValue: zIndex1,
        easing: Easing.back(),
        duration: duration
      }),

      Animated.timing(this.registerAnimatedScale, {
        toValue: scale2,
        easing: Easing.back(),
        duration: duration
      }),
      Animated.timing(this.registerAnimatedZIndex, {
        toValue: zIndex2,
        easing: Easing.back(),
        duration: duration
      })
    ]).start();
  }

  signIn() {
    const { signInEmail, signInPassword } = this.state;
    const data = qs.stringify({
      username: signInEmail,
      password: signInPassword
    });

    if (!signInEmail.trim() || !signInPassword.trim()) {
      this.alertError(err);
      return;
    } else
      axios
        .post(apiConfig.serverURL + "api/auth/signin", data, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        })
        .then(response => {
          onSignIn(response.data.accessToken);

          this.props.handleSignIn();
        })
        .catch(err => {
          this.alertError(err);

          resolve(false);
        });
  }

  render() {
    const signInAnimatedStyle = {
      transform: [{ scale: this.signInAnimatedScale }, { translateY: -230 }],
      zIndex: this.signInAnimatedZIndex
    };

    const registerAnimatedStyle = {
      transform: [{ scale: this.registerAnimatedScale }, { translateY: 220 }],
      zIndex: this.registerAnimatedZIndex
    };

    return (
      <KeyboardAvoidingView
        key={this.state.keyboardAvoidingViewKey}
        style={styles.lsContainer}
        behavior="height"
        enabled
      >
        {/* <View style={styles.lsContainer}> */}
        <LinearGradient
          colors={["#4c669f", "#3b5998", "#192f6a"]}
          style={{ padding: 15, alignItems: "center", borderRadius: 5 }}
        />
        <TouchableWithoutFeedback onPress={this.signInPress}>
          <Animated.View
            elevation={5}
            style={[styles.lsBlockSignIn, styles.lsBlock, signInAnimatedStyle]}
          >
            <View>
              <View style={[styles.lsBlockHeader, { marginBottom: 30 }]}>
                <Text style={styles.lsBlockHeaderText}>SIGN IN</Text>
              </View>
              <View style={styles.lsBlockContent}>
                <Text style={styles.lsLabel}>EMAIL</Text>
                <TextInput
                  style={[styles.lsSignInInputEmail, styles.lsInput]}
                  onChangeText={text => this.setState({ signInEmail: text })}
                  value={this.state.signInEmail}
                />

                <Text style={styles.lsLabel}>PASSWORD</Text>
                <View style={[styles.lsSignInViewPassword, styles.inlineBlock]}>
                  <TextInput
                    style={[styles.lsSignInInputPassword, styles.lsInput]}
                    onChangeText={text =>
                      this.setState({ signInPassword: text })
                    }
                    password={true}
                    secureTextEntry={!this.state.showSignInPassword}
                    value={this.state.signInPassword}
                  />

                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.visibilityBtn}
                    onPress={() => {
                      this.setState({
                        showSignInPassword: !this.state.showSignInPassword
                      });
                    }}
                  >
                    <Icon.Ionicons
                      color={"#ccc"}
                      name={
                        this.state.showSignInPassword
                          ? Platform.OS === "ios"
                            ? "ios-eye-off"
                            : "md-eye-off"
                          : Platform.OS === "ios"
                          ? "ios-eye"
                          : "md-eye"
                      }
                      size={18}
                    />
                    <Text style={styles.lsShowPasswordText}>
                      {this.state.showSignInPassword ? "hide" : "show"}
                    </Text>
                  </TouchableOpacity>
                </View>

                <Text style={styles.lsForgotPasswordText}>
                  Forgot password?
                </Text>

                <TouchableHighlight
                  onPress={this.signIn}
                  style={[styles.lsBtSignIn, styles.lsButton]}
                >
                  <Text style={[styles.textButton, { color: "#fff" }]}>
                    SIGN IN
                  </Text>
                </TouchableHighlight>
              </View>
            </View>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={this.registerPress}>
          <Animated.View
            elevation={5}
            style={[
              styles.lsBlockRegister,
              styles.lsBlock,
              registerAnimatedStyle
            ]}
          >
            <View>
              <View style={[styles.lsBlockHeader, { marginBottom: 10 }]}>
                <Text style={styles.lsBlockHeaderText}>CREATE NEW</Text>
              </View>
              <View style={styles.lsBlockContent}>
                <Text style={styles.lsLabel}>EMAIL</Text>
                <TextInput
                  style={[styles.lsRegisterInputEmail, styles.lsInput]}
                  onChangeText={text => this.setState({ registerEmail: text })}
                  value={this.state.registerEmail}
                />

                <Text style={styles.lsLabel}>PASSWORD</Text>
                <View style={[styles.inlineBlock]}>
                  <TextInput
                    style={[styles.lsRegisterInputPassword, styles.lsInput]}
                    onChangeText={text =>
                      this.setState({ registerPassword: text })
                    }
                    password={true}
                    secureTextEntry={!this.state.showRegisterPassword}
                    value={this.state.registerPassword}
                  />
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.visibilityBtn}
                    onPress={() => {
                      this.setState({
                        showRegisterPassword: !this.state.showRegisterPassword
                      });
                    }}
                  >
                    <Icon.Ionicons
                      color={"#ccc"}
                      name={
                        this.state.showRegisterPassword
                          ? Platform.OS === "ios"
                            ? "ios-eye-off"
                            : "md-eye-off"
                          : Platform.OS === "ios"
                          ? "ios-eye"
                          : "md-eye"
                      }
                      size={18}
                    />
                    <Text style={styles.lsShowPasswordText}>
                      {this.state.showRegisterPassword ? "hide" : "show"}
                    </Text>
                  </TouchableOpacity>
                </View>

                <Text style={styles.lsLabel}>RE-ENTER PASSWORD</Text>
                <View style={[styles.inlineBlock]}>
                  <TextInput
                    style={[styles.lsRegisterInputRepassword, styles.lsInput]}
                    onChangeText={text =>
                      this.setState({ registerRepassword: text })
                    }
                    password={true}
                    secureTextEntry={!this.state.showRegisterRepassword}
                    value={this.state.registerRepassword}
                  />
                  <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.visibilityBtn}
                    onPress={() => {
                      this.setState({
                        showRegisterRepassword: !this.state
                          .showRegisterRepassword
                      });
                    }}
                  >
                    <Icon.Ionicons
                      color={"#ccc"}
                      name={
                        this.state.showRegisterRepassword
                          ? Platform.OS === "ios"
                            ? "ios-eye-off"
                            : "md-eye-off"
                          : Platform.OS === "ios"
                          ? "ios-eye"
                          : "md-eye"
                      }
                      size={18}
                    />
                    <Text style={styles.lsShowPasswordText}>
                      {this.state.showRegisterRepassword ? "hide" : "show"}
                    </Text>
                  </TouchableOpacity>
                </View>

                <TouchableHighlight
                  onPress={this.register}
                  style={[styles.lsBtRegister, styles.lsButton]}
                >
                  <Text style={[styles.textButton]}>CREATE NEW</Text>
                </TouchableHighlight>
              </View>
            </View>
          </Animated.View>
        </TouchableWithoutFeedback>
        {/* </View> */}
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  lsContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    //backgroundColor: '#2196F3',
    marginTop: Platform.OS === "ios" ? 0 : Constants.statusBarHeight
  },
  container: {
    //backgroundColor: '#2196F3',
    marginTop: Platform.OS === "ios" ? 0 : Constants.statusBarHeight,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  lsBlock: {
    backgroundColor: Colors.bgColor,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    height: 330,
    paddingTop: 20,
    paddingRight: 20,
    paddingBottom: 40,
    paddingLeft: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 50,
      height: 50
    },
    shadowOpacity: 0.5,
    shadowRadius: 200,
    width: width - 60
  },
  lsBlockSignIn: {
    position: "absolute",
    top: "50%",
    zIndex: 1
  },
  lsBlockSignInShowing: {
    //position: 'absolute',
    //top: '10%',
    zIndex: 2
  },
  lsBlockRegister: {
    position: "absolute",
    bottom: "50%",
    zIndex: 1
  },
  lsBlockRegisterShowing: {
    //position: 'absolute',
    //top: '22%',
    zIndex: 2
  },
  lsBlockHeader: {},
  lsBlockHeaderText: {
    fontSize: 22,
    fontWeight: "bold"
  },
  lsBlockContent: {},
  lsButton: {
    alignSelf: "center",
    marginTop: 20
  },
  lsBtSignIn: {
    justifyContent: "center",
    backgroundColor: Colors.brick,
    borderRadius: 4,
    alignItems: "center"
  },
  lsBtRegister: {},
  lsForgotPasswordText: {
    alignSelf: "flex-end",
    color: "#2196F3",
    fontWeight: "bold"
  },
  lsInput: {
    borderColor: "#ccc",
    borderRadius: 4,
    borderWidth: 1,
    height: 34,
    marginBottom: 10,
    paddingLeft: 10,
    paddingRight: 75
  },
  lsLabel: {
    color: "#ccc",
    fontWeight: "bold",
    marginBottom: 5
  },
  lsSignInInputEmail: {},
  lsSignInInputPassword: {},
  lsSignInViewPassword: {},
  inlineBlock: {
    position: "relative",
    alignSelf: "stretch",
    justifyContent: "center"
  },
  lsRegisterInputEmail: {},
  lsRegisterInputPassword: {},
  lsRegisterInputRepassword: {},
  lsShowPasswordText: {
    color: "#ccc",
    marginLeft: 5
  },
  visibilityBtn: {
    flexDirection: "row",
    position: "absolute",
    right: 3,
    height: 40,
    width: 65,
    padding: 5
  },
  textButton: {
    fontSize: 18,
    fontWeight: "bold",
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10
  }
});

export default connect(
  mapStateToProps,
  actions
)(LoginScreen);
// export default connect(mapStateToProps, null)(LoginScreen)
