import React from "react";
import {
  Image,
  
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";

import { SearchBar, Input } from "react-native-elements";
import Colors from "../constants/Colors";
import { Constants, Font, Icon } from "expo";

export default class SecondScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerTop}>
            <Text style={styles.hsHeaderTopLine1}>ALL INVOICES</Text>
            <View style={styles.hsHeaderTopLine2}>
              <Text style={styles.hsHeaderTopLine2Currency}>$</Text>
              <Text style={styles.hsHeaderTopLine2Text}>21,475</Text>
            </View>
          </View>
          <View style={styles.headerCenter}>
            <View
              style={[
                styles.headerCenterBlock,
                { backgroundColor: Colors.tintColor }
              ]}
            >
              <View style={styles.headerCenterBlockIcon}>
                <Icon.Ionicons
                  color={Colors.bgColor}
                  name={"ios-list"}
                  size={36}
                />
              </View>
              <Text
                style={[
                  styles.headerCenterBlockText,
                  { color: Colors.bgColor }
                ]}
              >
                4
              </Text>
            </View>
            <View
              style={[
                styles.headerCenterBlock,
                { backgroundColor: Colors.tea2 }
              ]}
            >
              <View style={styles.headerCenterBlockIcon}>
                <Icon.MaterialCommunityIcons
                  color={Colors.sea}
                  name={"check"}
                  size={36}
                />
              </View>
              <Text
                style={[styles.headerCenterBlockText, { color: Colors.sea }]}
              >
                1
              </Text>
            </View>
            <View
              style={[
                styles.headerCenterBlock,
                { backgroundColor: Colors.pearlRiver2 }
              ]}
            >
              <View style={styles.headerCenterBlockIcon}>
                <Icon.AntDesign
                  color={Colors.blue1}
                  name={"calendar"}
                  size={32}
                />
              </View>
              <Text
                style={[styles.headerCenterBlockText, { color: Colors.blue1 }]}
              >
                2
              </Text>
            </View>
            <View
              style={[
                styles.headerCenterBlock,
                { backgroundColor: Colors.lemonade2 }
              ]}
            >
              <View style={styles.headerCenterBlockIcon}>
                <Icon.MaterialCommunityIcons
                  color={Colors.brick}
                  name={"exclamation"}
                  size={40}
                />
              </View>
              <Text
                style={[styles.headerCenterBlockText, { color: Colors.brick }]}
              >
                1
              </Text>
            </View>
          </View>
          <View style={styles.headerBottom}>
            <SearchBar
              containerStyle={styles.ssSearchbarContainer}
              inputContainerStyle={styles.ssSearchbarInputContainer}
              inputStyle={styles.ssSearchbar}
              leftIconContainerStyle={styles.ssLeftIconContainer}
              lightTheme
              onChangeText={() => {}}
              placeholder="Search by id, name, task"
              icon={{ type: 'material', color: '#86939e', name: 'search', style:{ fontSize: 30, top: 15, left: 5} }}
            />
          </View>
        </View>
        <View style={styles.main}>
          <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[
                styles.ssBlockIcon,
                { backgroundColor: Colors.pearlRiver2 }
              ]}
            >
              <Icon.AntDesign
                color={Colors.blue1}
                name={"calendar"}
                size={24}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>Carve Digital</Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={"#ccc"} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine2Text1}>10/21</Text>
                <Text style={styles.ssBlockInfoLine2Text2}>Week 41 Design</Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>4,500</Text>
              </View>
            </View>
          </View>
          <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[
                styles.ssBlockIcon,
                { backgroundColor: Colors.pearlRiver2 }
              ]}
            >
              <Icon.AntDesign
                color={Colors.blue1}
                name={"calendar"}
                size={24}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>Pied Piper</Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={Colors.grey1} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine2Text1}>10/17</Text>
                <Text style={styles.ssBlockInfoLine2Text2}>Dashboard</Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>6,950</Text>
              </View>
            </View>
          </View>
          <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[
                styles.ssBlockIcon,
                { backgroundColor: Colors.lemonade2 }
              ]}
            >
              <Icon.MaterialCommunityIcons
                color={Colors.brick}
                name={"exclamation"}
                size={30}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>
                  Fintech Startup Inc.
                </Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={"#ccc"} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text
                  style={[
                    styles.ssBlockInfoLine2Text1,
                    { color: Colors.brick }
                  ]}
                >
                  10/12
                </Text>
                <Text style={styles.ssBlockInfoLine2Text2}>
                  Portfolio & Dash...
                </Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>5,650</Text>
              </View>
            </View>
          </View>
          <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[styles.ssBlockIcon, { backgroundColor: Colors.tea2 }]}
            >
              <Icon.MaterialCommunityIcons
                color={Colors.sea}
                name={"check"}
                size={36}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>
                  The Ideal Client
                </Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={Colors.grey1} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine2Text1}>10/07</Text>
                <Text style={styles.ssBlockInfoLine2Text2}>Week 40 Design</Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>3,850</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgColor,
    marginTop: Platform.OS === "ios" ? 0 : Constants.statusBarHeight,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  header: {},
  headerTop: {
    flex: 1,
    alignItems: "center"
  },
  hsHeaderTopLine1: {
    color: Colors.grey1,
    fontSize: 18,
    fontFamily: "Nunito-bold"
  },
  hsHeaderTopLine2: {
    flexDirection: "row",
    alignSelf: "center"
  },
  hsHeaderTopLine2Text: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 50
  },
  hsHeaderTopLine2Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 14,
    marginRight: 5
  },
  headerCenter: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
    marginBottom: 20
  },
  headerCenterBlock: {
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    width: "20%"
  },
  headerCenterBlockIcon: {
    paddingTop: 10,
    paddingBottom: 5
  },
  headerCenterBlockText: {
    fontFamily: "Nunito-bold",
    fontSize: 14
  },
  headerBottom: {},
  main: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "flex-start",
    marginTop: 40
  },
  ssBlock: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    paddingBottom: 30
  },
  ssBlockIcon: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    height: 50,
    width: 50,
    marginRight: 10
  },
  ssInlineBlock: {
    flexDirection: "row"
  },
  ssSearchbarContainer: {
    backgroundColor: "transparent",
    borderTopWidth: 0,
    borderBottomWidth: 0,
    padding: 0
  },
  ssLeftIconContainer: {},
  ssSearchbarInputContainer: {},
  ssSearchbar: {
    backgroundColor: "transparent",
    fontFamily: "Nunito",
    fontSize: 16,
    paddingLeft: 30
  },
  ssSearchIcon: {
    // fontSize: 44
  },
  ssBlockInfo: {
    flexDirection: "column",
    alignItems: "flex-start"
  },
  ssBlockInfoLine1: {
    alignItems: "center"
  },
  ssBlockInfoLine1Text: {
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 20,
    marginRight: 5
  },
  ssBlockInfoLine1Icon: {
    alignSelf: "center"
  },
  ssBlockInfoLine2: {
    alignItems: "center"
  },
  ssBlockInfoLine2Text1: {
    color: Colors.tintColor,
    fontFamily: "Nunito",
    fontSize: 13,
    marginRight: 5
  },
  ssBlockInfoLine2Text2: {
    color: Colors.tintColor,
    fontFamily: "Nunito",
    fontSize: 14,
    marginRight: 5
  },
  ssBlockInfoLine2Text3Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 11
  },
  ssBlockInfoLine2Text3: {
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 15
  }
});
