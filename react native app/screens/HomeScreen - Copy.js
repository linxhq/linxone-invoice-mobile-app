import React from "react";
import {
  ActivityIndicator,
  Alert,
  FlatList,
  Image,
  ListView,
  Platform,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";

import apiConfig from "../config/api";
import { SearchBar, Input } from "react-native-elements";
import Colors from "../constants/Colors";
import { Constants, Font, Icon } from "expo";
import axios from "axios";
import qs from "qs";
import { getAccessToken } from "../auth";
import { addComma, currencyFormat } from "../lib/number";
import moment from "moment";

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      isLoading: true,
      payment: [],
      invoices: [],
      refreshing: false,
      waitingMoreInvoices: false,
      invoicesOffset: 0,
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
    };

    this.renderList = this.renderList.bind(this);
    this.onEndReached = this.onEndReached.bind(this);
    this.getMoreInvoices = this.getMoreInvoices.bind(this);
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.fetchData();
  }
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.fetchData().then(() => {
      this.setState({ refreshing: false });
    });
  };

  fetchData = () => {
    return new Promise((resolve, reject) => {
      getAccessToken()
        .then(res => {
          axios
            .get(apiConfig.serverURL + "api/home", {
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "x-access-token":
                  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoxLCJpYXQiOjE1NDQwMDMzNjYsImV4cCI6MTU0NDA4OTc2Nn0.wJfuHRPhbqkCsSSCT_xvzyRpnJsUJ4IjrFZaQjY_96o"
              }
            })
            .then(response => {
              this.setState({
                payment: response.data.payment,
                invoices: response.data.invoices,
                dataSource: this.state.dataSource.cloneWithRows(
                  response.data.invoices
                )
              });
              this.setState({ isLoading: false });
              resolve(true);
            })
            .catch(err => {
              alert("An error occurred 1");
              resolve(false);
            });
        })
        .catch(err => alert("An error occurred 2"));
    });
  };

  getMoreInvoices() {
    return new Promise((resolve, reject) => {
      getAccessToken()
        .then(res => {
          axios
            .get(apiConfig.serverURL + "api/moreinvoices", {
              params: { offset: this.state.invoicesOffset + 10 },
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "x-access-token":
                  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoxLCJpYXQiOjE1NDQwMDMzNjYsImV4cCI6MTU0NDA4OTc2Nn0.wJfuHRPhbqkCsSSCT_xvzyRpnJsUJ4IjrFZaQjY_96o"
              }
            })
            .then(response => {
              if (response.data.length != 0) {
                this.setState({
                  invoices: this.state.invoices.concat(response.data),
                  waitingMoreInvoices: false,
                  invoicesOffset: this.state.invoicesOffset + 10,
                  dataSource: this.state.dataSource.cloneWithRows(
                    this.state.invoices.concat(response.data)
                  )
                });

                resolve(true);
              } else {
                Alert.alert(
                  "Alert",
                  "All records were loaded.",
                  [
                    {
                      text: "Cancel",
                      onPress: () => console.log("Cancel Pressed"),
                      style: "cancel"
                    },
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                  ],
                  { cancelable: false }
                );
                resolve(false);
              }
            })
            .catch(err => {
              alert("An error occurred 3");
              resolve(false);
            });
        })
        .catch(err => alert("An error occurred 4"));
    });
  }

  onEndReached() {
    if (
      !this.state.isLoading &&
      !this.state.refreshing &&
      !this.state.waitingMoreInvoices
    ) {
      this.setState({ waitingMoreInvoices: true });
      this.getMoreInvoices();
    }
  }
  renderFooter = waitingMoreInvoices => {
    if (waitingMoreInvoices) {
      return <ActivityIndicator />;
    } else {
      return <Text>~</Text>;
    }
  };

  renderList = (invoices, waitingMoreInvoices) => {
    if (!invoices.length) {
      return (
        <View>
          <Text>NO INVOICES TO LIST</Text>
        </View>
      );
    }
    return (
      <FlatList
      data={this.state.invoices}
      onEndReached={this.onEndReached}
      keyExtractor={(item, index) => index.toString()}
      renderItem={(invoice, index) => {
        return (
          <View style={[styles.ssBlock, styles.ssInlineBlock]} id={invoice.item.lb_record_primary_key}>
            <View
              style={[
                styles.ssBlockIcon,
                { backgroundColor: Colors.pearlRiver2 }
              ]}
            >
              <Icon.AntDesign
                color={Colors.blue1}
                name={"calendar"}
                size={24}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>
                  {invoice.item.lb_customer_name || "--"}
                  {invoice.item.lb_record_primary_key}
                </Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={"#ccc"} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine2Text1}>
                  {moment(invoice.item.lb_invoice_due_date).format("MM/DD") ||
                    "--"}
                </Text>
                <Text style={styles.ssBlockInfoLine2Text2}>
                  {invoice.item.lb_invoice_subject || "--"}
                </Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>
                  {invoice.item.lb_invoice_total_after_taxes ? "$" : ""}
                </Text>
                <Text style={styles.ssBlockInfoLine2Text3}>
                  {addComma(invoice.item.lb_invoice_total_after_taxes) || "--"}
                </Text>
              </View>
            </View>
          </View>
        );
      }}
      />
      // <ListView
      //   dataSource={this.state.dataSource}
      //   renderFooter={waitingMoreInvoices => {
      //     if (waitingMoreInvoices) {
      //       return <ActivityIndicator />;
      //     } else {
      //       return <Text>~</Text>;
      //     }
      //   }}
      //   onEndReached={this.onEndReached}
      //   // onEndReachedThreshold ={0}
      //   renderRow={invoice => {
      //     return (
      //       <View style={[styles.ssBlock, styles.ssInlineBlock]}>
      //         <View
      //           style={[
      //             styles.ssBlockIcon,
      //             { backgroundColor: Colors.pearlRiver2 }
      //           ]}
      //         >
      //           <Icon.AntDesign
      //             color={Colors.blue1}
      //             name={"calendar"}
      //             size={24}
      //           />
      //         </View>
      //         <View style={styles.ssBlockInfo}>
      //           <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
      //             <Text style={styles.ssBlockInfoLine1Text}>
      //               {invoice.lb_customer_name || "--"}
      //               {invoice.lb_record_primary_key}
      //             </Text>
      //             <View style={styles.ssBlockInfoLine1Icon}>
      //               <Icon.Feather color={"#ccc"} name={"eye"} size={16} />
      //             </View>
      //           </View>
      //           <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
      //             <Text style={styles.ssBlockInfoLine2Text1}>
      //               {moment(invoice.lb_invoice_due_date).format("MM/DD") ||
      //                 "--"}
      //             </Text>
      //             <Text style={styles.ssBlockInfoLine2Text2}>
      //               {invoice.lb_invoice_subject || "--"}
      //             </Text>
      //             <Text style={styles.ssBlockInfoLine2Text3Currency}>
      //               {invoice.lb_invoice_total_after_taxes ? "$" : ""}
      //             </Text>
      //             <Text style={styles.ssBlockInfoLine2Text3}>
      //               {addComma(invoice.lb_invoice_total_after_taxes) || "--"}
      //             </Text>
      //           </View>
      //         </View>
      //       </View>
      //     );
      //   }}
      // />
    );
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    const { payment, invoices } = this.state;

    return (
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
      >
        <View style={styles.header}>
          <View style={styles.headerTop}>
            <Text style={styles.hsHeaderTopLine1}>INCOME THIS YEAR</Text>
            <View style={styles.hsHeaderTopLine2}>
              <Text style={styles.hsHeaderTopLine2Currency}>$</Text>
              <Text style={styles.hsHeaderTopLine2Text}>
                {addComma(payment.totalThisYear)}
              </Text>
            </View>
          </View>
          <View style={styles.headerCenter}>
            <View style={styles.headerCenterSubInfo}>
              <View style={styles.hsHeaderCenterSubInfoLine1}>
                <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
                <Text style={styles.hsHeaderCenterSubInfoLine1Text}>
                  {currencyFormat(payment.totalBeforeLast)}
                </Text>
              </View>
              <Text style={styles.hsHeaderCenterSubInfoLine2}>
                {moment()
                  .subtract(2, "month")
                  .format("MMM")
                  .toUpperCase()}
              </Text>
            </View>
            <View style={styles.headerCenterSubInfo}>
              <View style={styles.hsHeaderCenterSubInfoLine1}>
                <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
                <Text style={styles.hsHeaderCenterSubInfoLine1Text}>
                  {currencyFormat(payment.totalLastMonth)}
                </Text>
              </View>
              <Text style={styles.hsHeaderCenterSubInfoLine2}>
                {moment()
                  .subtract(1, "month")
                  .format("MMM")
                  .toUpperCase()}
              </Text>
            </View>
            <View style={styles.headerCenterSubInfo}>
              <View style={styles.hsHeaderCenterSubInfoLine1}>
                <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
                <Text style={styles.hsHeaderCenterSubInfoLine1Text}>
                  {currencyFormat(payment.totalLastYear)}
                </Text>
              </View>
              <Text style={styles.hsHeaderCenterSubInfoLine2}>
                {moment()
                  .subtract(1, "year")
                  .format("YYYY")}
              </Text>
            </View>
          </View>
          <View style={styles.headerBottom}>
            {/* <Chart > */}
            <View style={styles.headerBottomSearchbar}>
              <SearchBar
                containerStyle={styles.ssSearchbarContainer}
                inputContainerStyle={styles.ssSearchbarInputContainer}
                inputStyle={styles.ssSearchbar}
                leftIconContainerStyle={styles.ssLeftIconContainer}
                lightTheme
                onChangeText={() => {}}
                placeholder="Search by id, name, task"
                icon={{
                  type: "material",
                  color: "#86939e",
                  name: "search",
                  style: { fontSize: 30, top: 15, left: 5 }
                }}
              />
            </View>
          </View>
        </View>
        <View style={styles.main}>
          {this.renderList(invoices, this.state.waitingMoreInvoices)}

          {/* <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[
                styles.ssBlockIcon,
                { backgroundColor: Colors.pearlRiver2 }
              ]}
            >
              <Icon.AntDesign
                color={Colors.blue1}
                name={"calendar"}
                size={24}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>Carve Digital</Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={"#ccc"} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine2Text1}>10/21</Text>
                <Text style={styles.ssBlockInfoLine2Text2}>Week 41 Design</Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>4,500</Text>
              </View>
            </View>
          </View>
          <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[
                styles.ssBlockIcon,
                { backgroundColor: Colors.pearlRiver2 }
              ]}
            >
              <Icon.AntDesign
                color={Colors.blue1}
                name={"calendar"}
                size={24}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>Pied Piper</Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={Colors.grey1} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine2Text1}>10/17</Text>
                <Text style={styles.ssBlockInfoLine2Text2}>Dashboard</Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>6,950</Text>
              </View>
            </View>
          </View>
          <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[
                styles.ssBlockIcon,
                { backgroundColor: Colors.lemonade2 }
              ]}
            >
              <Icon.MaterialCommunityIcons
                color={Colors.brick}
                name={"exclamation"}
                size={30}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>
                  Fintech Startup Inc.
                </Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={"#ccc"} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text
                  style={[
                    styles.ssBlockInfoLine2Text1,
                    { color: Colors.brick }
                  ]}
                >
                  10/12
                </Text>
                <Text style={styles.ssBlockInfoLine2Text2}>
                  Portfolio & Dash...
                </Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>5,650</Text>
              </View>
            </View>
          </View>
          <View style={[styles.ssBlock, styles.ssInlineBlock]}>
            <View
              style={[styles.ssBlockIcon, { backgroundColor: Colors.tea2 }]}
            >
              <Icon.MaterialCommunityIcons
                color={Colors.sea}
                name={"check"}
                size={36}
              />
            </View>
            <View style={styles.ssBlockInfo}>
              <View style={[styles.ssBlockInfoLine1, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine1Text}>
                  The Ideal Client
                </Text>
                <View style={styles.ssBlockInfoLine1Icon}>
                  <Icon.Feather color={Colors.grey1} name={"eye"} size={16} />
                </View>
              </View>
              <View style={[styles.ssBlockInfoLine2, styles.ssInlineBlock]}>
                <Text style={styles.ssBlockInfoLine2Text1}>10/07</Text>
                <Text style={styles.ssBlockInfoLine2Text2}>Week 40 Design</Text>
                <Text style={styles.ssBlockInfoLine2Text3Currency}>$</Text>
                <Text style={styles.ssBlockInfoLine2Text3}>3,850</Text>
              </View>
            </View>
          </View> */}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgColor,
    marginTop: Platform.OS === "ios" ? 0 : Constants.statusBarHeight,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  header: {},
  headerTop: {
    flex: 1,
    alignItems: "center"
  },
  hsHeaderTopLine1: {
    color: Colors.grey1,
    fontSize: 18,
    fontFamily: "Nunito-bold"
  },
  hsHeaderTopLine2: {
    flexDirection: "row",
    alignSelf: "center"
  },
  hsHeaderTopLine2Text: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    // fontSize: 50
    fontSize: 44
  },
  hsHeaderTopLine2Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 14,
    marginRight: 5
  },
  headerCenter: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20
  },
  headerCenterSubInfo: {},
  hsHeaderCenterSubInfoLine1: {
    flexDirection: "row",
    alignSelf: "center"
  },
  hsHeaderCenterSubInfoLine1Text: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 20
  },
  hsHeaderCenterSubInfoLine1Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 12,
    marginRight: 3
  },
  hsHeaderCenterSubInfoLine2: {
    alignSelf: "center",
    color: "#777",
    fontFamily: "Nunito-bold",
    fontSize: 14
  },
  headerBottom: {},
  headerBottomSearchbar: {
    marginTop: 80,
    paddingLeft: 10,
    paddingRight: 10
  },
  // main: {
  //   flex: 1,
  //   flexDirection: "row",
  //   flexWrap: "wrap",
  //   justifyContent: "space-around",
  //   marginTop: 100
  // },
  block: {
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    // marginBottom: 30,
    paddingBottom: 30
  },
  hsBlockLine1: {
    height: 40
  },
  hsBlockLine2: {
    color: Colors.grey1,
    fontFamily: "Nunito-bold",
    fontSize: 12,

    marginTop: 5,
    marginBottom: 0
  },
  hsBlockLine3: {
    color: Colors.tintColor,
    fontSize: 30,
    fontFamily: "Nunito-bold",
    marginBottom: 0
  },
  hsBlockLine3Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 12,
    marginRight: 3
  },
  hsBlock3Line3SubBlock: {
    alignSelf: "center",
    backgroundColor: Colors.brick,
    borderRadius: 3,
    color: Colors.bgColor,
    height: 22,
    fontFamily: "Nunito-bold",
    marginLeft: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  hsBlockLine4: {
    color: "grey",
    fontFamily: "Nunito",
    fontSize: 11
  },
  hsBlock4Line3SubBlock: {
    alignSelf: "center",
    backgroundColor: Colors.sea,
    borderRadius: 3,
    color: Colors.bgColor,
    height: 22,
    fontFamily: "Nunito-bold",
    marginLeft: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  hsInlineBlock: {
    flexDirection: "row",
    alignSelf: "center",
    marginBottom: 5
  },
  main: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "flex-start",
    marginTop: 40,
    paddingLeft: 20,
    paddingRight: 20
  },
  ssBlock: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    paddingBottom: 30
  },
  ssBlockIcon: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    height: 50,
    width: 50,
    marginRight: 10
  },
  ssInlineBlock: {
    flexDirection: "row"
  },
  ssSearchbarContainer: {
    backgroundColor: "transparent",
    borderTopWidth: 0,
    borderBottomWidth: 0,
    padding: 0
  },
  ssLeftIconContainer: {},
  ssSearchbarInputContainer: {},
  ssSearchbar: {
    backgroundColor: "transparent",
    fontFamily: "Nunito",
    fontSize: 16,
    paddingLeft: 30
  },
  ssSearchIcon: {
    // fontSize: 44
  },
  ssBlockInfo: {
    flexDirection: "column",
    alignItems: "flex-start"
  },
  ssBlockInfoLine1: {
    alignItems: "center"
  },
  ssBlockInfoLine1Text: {
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 20,
    marginRight: 5
  },
  ssBlockInfoLine1Icon: {
    alignSelf: "center"
  },
  ssBlockInfoLine2: {
    alignItems: "center"
  },
  ssBlockInfoLine2Text1: {
    color: Colors.tintColor,
    fontFamily: "Nunito",
    fontSize: 13,
    marginRight: 5
  },
  ssBlockInfoLine2Text2: {
    color: Colors.tintColor,
    fontFamily: "Nunito",
    fontSize: 14,
    marginRight: 5
  },
  ssBlockInfoLine2Text3Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 11
  },
  ssBlockInfoLine2Text3: {
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 15
  }
});
