import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";

import Colors from "../constants/Colors";
import { Constants, Font, Icon } from "expo";

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerTop}>
            <Text style={styles.hsHeaderTopLine1}>INCOME THIS WEEK</Text>
            <View style={styles.hsHeaderTopLine2}>
              <Text style={styles.hsHeaderTopLine2Currency}>$</Text>
              <Text style={styles.hsHeaderTopLine2Text}>10,925</Text>
            </View>
          </View>
          <View style={styles.headerCenter}>
            <View style={styles.headerCenterSubInfo}>
              <View style={styles.hsHeaderCenterSubInfoLine1}>
                <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
                <Text style={styles.hsHeaderCenterSubInfoLine1Text}>33.6k</Text>
              </View>
              <Text style={styles.hsHeaderCenterSubInfoLine2}>SEP</Text>
            </View>
            <View style={styles.headerCenterSubInfo}>
              <View style={styles.hsHeaderCenterSubInfoLine1}>
                <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
                <Text style={styles.hsHeaderCenterSubInfoLine1Text}>19.8k</Text>
              </View>
              <Text style={styles.hsHeaderCenterSubInfoLine2}>OCT</Text>
            </View>
            <View style={styles.headerCenterSubInfo}>
              <View style={styles.hsHeaderCenterSubInfoLine1}>
                <Text style={styles.hsHeaderCenterSubInfoLine1Currency}>$</Text>
                <Text style={styles.hsHeaderCenterSubInfoLine1Text}>
                  302.9k
                </Text>
              </View>
              <Text style={styles.hsHeaderCenterSubInfoLine2}>2018</Text>
            </View>
          </View>
          <View style={styles.headerBottom}>{/* <Chart > */}</View>
        </View>
        <View style={styles.main}>
          <View style={styles.block}>
            <View style={styles.hsBlockLine1}>
              <Icon.Ionicons
                color={Colors.blue1}
                name={Platform.OS == "ios" ? "ios-alarm" : "md-alarm"}
                size={36}
              />
            </View>
            <Text style={styles.hsBlockLine2}>WEEKLY HOURS</Text>
            <Text style={styles.hsBlockLine3}>43.8</Text>
            <Text style={styles.hsBlockLine4}>7.3 Today</Text>
          </View>
          <View style={styles.block}>
            <View style={styles.hsBlockLine1}>
              <Icon.MaterialCommunityIcons
                color={Colors.blue1}
                name={Platform.OS == "ios" ? "calculator" : "calculator"}
                size={36}
              />
            </View>
            <Text style={styles.hsBlockLine2}>AVG. $/HOUR</Text>
            <View style={styles.hsInlineBlock}>
              <Text style={styles.hsBlockLine3Currency}>$</Text>
              <Text style={styles.hsBlockLine3}>213</Text>
            </View>
            <Text style={styles.hsBlockLine4}>$1,424 / Day</Text>
          </View>
          <View style={styles.block}>
            <View style={styles.hsBlockLine1}>
              <Icon.AntDesign
                color={Colors.blue1}
                name={Platform.OS == "ios" ? "filetext1" : "filetext1"}
                size={30}
              />
            </View>
            <Text style={styles.hsBlockLine2}>OPEN INVOICES</Text>

            <View style={styles.hsInlineBlock}>
              <Text style={styles.hsBlockLine3}>3</Text>
              <Text style={styles.hsBlock3Line3SubBlock}>1 Late</Text>
            </View>
            <Text style={styles.hsBlockLine4}>$13,801</Text>
          </View>
          <View style={styles.block}>
            <View style={styles.hsBlockLine1}>
              <Icon.MaterialCommunityIcons
                color={Colors.blue1}
                name={
                  Platform.OS == "ios"
                    ? "check-circle-outline"
                    : "check-circle-outline"
                }
                size={36}
              />
            </View>
            <Text style={styles.hsBlockLine2}>TASKS TODAY</Text>
            <View style={styles.hsInlineBlock}>
              <Text style={styles.hsBlockLine3}>11</Text>
              <Text style={styles.hsBlock4Line3SubBlock}>6</Text>
            </View>
            <Text style={styles.hsBlockLine4}>47 This Week</Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgColor,
    marginTop: Platform.OS === "ios" ? 0 : Constants.statusBarHeight,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20
  },
  header: {},
  headerTop: {
    flex: 1,
    alignItems: "center"
  },
  hsHeaderTopLine1: {
    color: Colors.grey1,
    fontSize: 18,
    fontFamily: "Nunito-bold"
  },
  hsHeaderTopLine2: {
    flexDirection: "row",
    alignSelf: "center"
  },
  hsHeaderTopLine2Text: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 50
  },
  hsHeaderTopLine2Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 14,
    marginRight: 5
  },
  headerCenter: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20
  },
  headerCenterSubInfo: {},
  hsHeaderCenterSubInfoLine1: {
    flexDirection: "row",
    alignSelf: "center"
  },
  hsHeaderCenterSubInfoLine1Text: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 20
  },
  hsHeaderCenterSubInfoLine1Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 12,
    marginRight: 3
  },
  hsHeaderCenterSubInfoLine2: {
    alignSelf: "center",
    color: "#777",
    fontFamily: "Nunito-bold",
    fontSize: 14
  },
  headerBottom: {},
  main: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-around",
    marginTop: 100
  },
  block: {
    width: "50%",
    alignItems: "center",
    justifyContent: "center",
    // marginBottom: 30,
    paddingBottom: 30
  },
  hsBlockLine1: {
    height: 40
  },
  hsBlockLine2: {
    color: Colors.grey1,
    fontFamily: "Nunito-bold",
    fontSize: 12,

    marginTop: 5,
    marginBottom: 0
  },
  hsBlockLine3: {
    color: Colors.tintColor,
    fontSize: 30,
    fontFamily: "Nunito-bold",
    marginBottom: 0
  },
  hsBlockLine3Currency: {
    alignSelf: "center",
    color: Colors.tintColor,
    fontFamily: "Nunito-bold",
    fontSize: 12,
    marginRight: 3
  },
  hsBlock3Line3SubBlock: {
    alignSelf: "center",
    backgroundColor: Colors.brick,
    borderRadius: 3,
    color: Colors.bgColor,
    height: 22,
    fontFamily: "Nunito-bold",
    marginLeft: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  hsBlockLine4: {
    color: "grey",
    fontFamily: "Nunito",
    fontSize: 11
  },
  hsBlock4Line3SubBlock: {
    alignSelf: "center",
    backgroundColor: Colors.sea,
    borderRadius: 3,
    color: Colors.bgColor,
    height: 22,
    fontFamily: "Nunito-bold",
    marginLeft: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  hsInlineBlock: {
    flexDirection: "row",
    alignSelf: "center",
    marginBottom: 5
  }
});
