import React from "react";
import { Platform } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";

import Colors from "../constants/Colors";

import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/HomeScreen";
import SecondScreen from "../screens/SecondScreen";
import SettingsScreen from "../screens/SettingsScreen";
import OthersScreen from "../screens/OthersScreen";

const HomeStack = createStackNavigator({
  Home: HomeScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: "Home",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      source="MaterialCommunityIcons"
      focused={focused}
      name={Platform.OS === "ios" ? `home-outline` : "home-outline"}
    />
  )
};

const SecondStack = createStackNavigator({
  Second: SecondScreen
});

SecondStack.navigationOptions = {
  tabBarLabel: "Second Screen",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      source="AntDesign"
      focused={focused}
      name={Platform.OS === "ios" ? "filetext1" : "filetext1"}
    />
  )
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen
});

SettingsStack.navigationOptions = {
  tabBarLabel: "Settings",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      activeColor={"red"}
      defaultColor={Colors.brick}
      source="MaterialCommunityIcons"
      focused={focused}
      name={
        Platform.OS === "ios"
          ? "arrow-right-drop-circle"
          : "arrow-right-drop-circle"
      }
    />
  )
};

const OthersStack = createStackNavigator({
  Others: OthersScreen
});

OthersStack.navigationOptions = {
  tabBarLabel: "Others",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      source={"Entypo"}
      focused={focused}
      name={
        Platform.OS === "ios"
          ? "dots-three-horizontal"
          : "dots-three-horizontal"
      }
    />
  )
};

export default createBottomTabNavigator(
  {
    HomeStack,
    SecondStack,
    SettingsStack,
    OthersStack
  },
  {
    initialRouteName: 'HomeStack',
    tabBarOptions: {
      style: {
        backgroundColor: Colors.bottomNavBgColor,
        height: 60
      },
      showLabel: false
    }
  }
);
