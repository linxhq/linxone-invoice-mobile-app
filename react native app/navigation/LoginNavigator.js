import React from 'react';
import { createStackNavigator } from 'react-navigation';

import LoginScreen from "../screens/LoginScreen";

const Login = ({ navigation }) => (<LoginScreen navigation={navigation}/>);

export default createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    }
});
