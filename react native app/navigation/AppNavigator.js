import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import LoginNavigator from "./LoginNavigator";

export const AppNavigator = (signedIn = false) => {
    return createSwitchNavigator(
        {
            Main: MainTabNavigator,
            Login: LoginNavigator
        },
        {
            initialRouteName: signedIn ? "Main" : "Login"
        }
    );
};
