import React from "react";
import { Icon } from "expo";

import Colors from "../constants/Colors";

export default class TabBarIcon extends React.Component {
  render() {
    switch (this.props.source) {
      case "AntDesign":
        return (
          <Icon.AntDesign
            name={this.props.name}
            size={22}
            style={{ marginBottom: -3 }}
            color={
              this.props.focused
                ? this.props.activeColor
                  ? this.props.activeColor
                  : Colors.tabIconSelected
                : this.props.defaultColor
                  ? this.props.defaultColor
                  : Colors.tabIconDefault
            }
          />
        );
        break;
      case "Entypo":
        return (
          <Icon.Entypo
            name={this.props.name}
            size={30}
            style={{ marginBottom: -3 }}
            color={
              this.props.focused
                ? this.props.activeColor
                  ? this.props.activeColor
                  : Colors.tabIconSelected
                : this.props.defaultColor
                  ? this.props.defaultColor
                  : Colors.tabIconDefault
            }
          />
        );
        break;
      case "FontAwesome":
        return (
          <Icon.FontAwesome
            name={this.props.name}
            size={30}
            style={{ marginBottom: -3 }}
            color={
              this.props.focused
                ? this.props.activeColor
                  ? this.props.activeColor
                  : Colors.tabIconSelected
                : this.props.defaultColor
                  ? this.props.defaultColor
                  : Colors.tabIconDefault
            }
          />
        );
        break;
      case "MaterialCommunityIcons":
        return (
          <Icon.MaterialCommunityIcons
            name={this.props.name}
            size={28}
            style={{ marginBottom: -3 }}
            color={
              this.props.focused
                ? this.props.activeColor
                  ? this.props.activeColor
                  : Colors.tabIconSelected
                : this.props.defaultColor
                  ? this.props.defaultColor
                  : Colors.tabIconDefault
            }
          />
        );
        break;

      default:
        return (
          <Icon.Ionicons
            name={this.props.name}
            size={26}
            style={{ marginBottom: -3 }}
            color={
              this.props.focused
                ? this.props.activeColor
                  ? this.props.activeColor
                  : Colors.tabIconSelected
                : this.props.defaultColor
                  ? this.props.defaultColor
                  : Colors.tabIconDefault
            }
          />
        );
        break;
    }
  }
}
