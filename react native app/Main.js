import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
// import * as actions from "./actions";
import { connect } from "react-redux";

import { AppNavigator } from "./navigation/AppNavigator";


class Main extends Component {
  render() {
    const Layout = AppNavigator(this.props.signIn);
    
    return <Layout />;
  }
}

const mapStateToProps = state => ({
    signIn: state.signIn
});
export default connect(
    mapStateToProps,
  null
)(Main);
