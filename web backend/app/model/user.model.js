module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define('lb_sys_accounts', {
		account_id: {
		  type: Sequelize.INTEGER,
		  primaryKey: true
		},
	  account_email: {
		  type: Sequelize.STRING
	  },
	  account_password: {
		  type: Sequelize.STRING
	  },
	   account_status: {
	  	type: Sequelize.INTEGER
	  },
	  check_user_activated: {
	  	type: Sequelize.INTEGER
	  }
	});
	
	return User;
}
