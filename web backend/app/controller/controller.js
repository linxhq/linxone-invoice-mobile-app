const db = require('../config/db.config.js');
const config = require('../config/config.js');
const User = db.user;
const Role = db.role;

const Op = db.Sequelize.Op;

var moment = require('moment');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const Sequelize = require('sequelize');

exports.signup = (req, res) => {
	// Save User to Database
	console.log("Processing func -> SignUp");
	
	User.create({
		name: req.body.name,
		username: req.body.username,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 8)
	}).then(user => {
		Role.findAll({
		  where: {
			name: {
			  [Op.or]: req.body.roles
			}
		  }
		}).then(roles => {
			user.setRoles(roles).then(() => {
				res.send("User registered successfully!");
            });
		}).catch(err => {
			res.status(500).send("Error -> " + err);
		});
	}).catch(err => {
		res.status(500).send("Fail! Error -> " + err);
	})
}

exports.signin = (req, res) => {
	console.log("Sign-In");

	User.findOne({
		where: {
			account_email: req.body.username
		}
	}).then(user => {

		if (!user) {
			return res.status(404).send('User Not Found.');
		}

		var passwordIsValid = bcrypt.compareSync(req.body.password, user.account_password);
		if (!passwordIsValid) {
			return res.status(401).send({ auth: false, accessToken: null, reason: "Invalid Password!" });
		}
		
		var token = jwt.sign({ account_id: user.account_id }, config.secret, {
		  expiresIn: 2592000 // expires in 1 month
		});

	    user.account_password = undefined;

		res.status(200).send({ auth: true, accessToken: token, user: user });
		
	}).catch(err => {
		res.status(500).send('Error -> ' + err);
	});
}

exports.home = (req, res) => {
	var result = {};

	var thisYear = moment().year();

	var lastMonthStart = moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
	var lastMonthEnd = moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
	var beforeLastStart = moment().subtract(2, 'months').startOf('month').format('YYYY-MM-DD');
	var beforeLastEnd = moment().subtract(2, 'months').endOf('month').format('YYYY-MM-DD');


	var sqlArr = [];

    sqlArr.push("SELECT SUM(lb_payment_total) as totalThisYear FROM lb_payment WHERE lb_payment_date >= '"+thisYear+"-01-01' AND lb_payment_date <= '"+thisYear+"-12-30'");

	sqlArr.push("SELECT SUM(lb_payment_total) as totalLastYear FROM lb_payment WHERE lb_payment_date >= '"+(thisYear-1)+"-01-01' AND lb_payment_date <= '"+(thisYear-1)+"-12-30'");

	sqlArr.push("SELECT SUM(lb_payment_total) as totalLastMonth FROM lb_payment WHERE lb_payment_date >= '"+lastMonthStart+"' AND lb_payment_date <= '"+lastMonthEnd+"'");

	sqlArr.push("SELECT SUM(lb_payment_total) as totalBeforeLast FROM lb_payment WHERE lb_payment_date >= '"+beforeLastStart+"' AND lb_payment_date <= '"+beforeLastEnd+"'");

	sqlArr.push("SELECT cu.lb_customer_name, inv.lb_record_primary_key, inv.lb_invoice_due_date, inv.lb_invoice_subject, inv.lb_invoice_total_after_taxes FROM lb_customers cu RIGHT JOIN (SELECT i.lb_record_primary_key, i.lb_invoice_customer_id, i.lb_invoice_due_date, i.lb_invoice_subject, it.lb_invoice_total_after_taxes FROM lb_invoices i LEFT JOIN lb_invoice_totals it ON i.lb_record_primary_key = it.lb_invoice_id) inv ON cu.lb_record_primary_key = inv.lb_invoice_customer_id ORDER BY lb_record_primary_key DESC LIMIT 10 ");

		

	db.sequelize.query(sqlArr.join(' ; '), { type: db.Sequelize.QueryTypes.SELECT})
	  .then(results => {
				result['payment'] = {
					totalThisYear: results[0][0]['totalThisYear'],
					totalLastYear: results[1][0]['totalLastYear'],
					totalLastMonth: results[2][0]['totalLastMonth'],
					totalBeforeLast: results[3][0]['totalBeforeLast'],
				};


				result['invoices'] = Object.values(results[4]);

		    	res.status(200).json(result);
	   
    })
}

exports.moreinvoices = (req, res) => {
	db.sequelize.query("SELECT cu.lb_customer_name, inv.lb_record_primary_key, inv.lb_invoice_due_date, inv.lb_invoice_subject, inv.lb_invoice_total_after_taxes FROM lb_customers cu RIGHT JOIN (SELECT i.lb_record_primary_key, i.lb_invoice_customer_id, i.lb_invoice_due_date, i.lb_invoice_subject, it.lb_invoice_total_after_taxes FROM lb_invoices i LEFT JOIN lb_invoice_totals it ON i.lb_record_primary_key = it.lb_invoice_id) inv ON cu.lb_record_primary_key = inv.lb_invoice_customer_id ORDER BY lb_record_primary_key DESC LIMIT 10 OFFSET " + req.query.offset, { type: db.Sequelize.QueryTypes.SELECT})
  .then(results => {


	    	res.status(200).json(Object.values(results));
   
  })
}

exports.userContent = (req, res) => {
	res.status(200).json({
			"description": "User Content Page",
			"user": 'user'
		});
	User.findOne({
		where: {account_id: req.userId},
		attributes: ['account_email'],
		// include: [{
		// 	model: Role,
		// 	attributes: ['id', 'name'],
		// 	through: {
		// 		attributes: ['userId', 'roleId'],
		// 	}
		// }]
	}).then(user => {
		res.status(200).json({
			"description": "User Content Page",
			"user": user
		});
	}).catch(err => {
		res.status(500).json({
			"description": "Can not access User Page",
			"error": err
		});
	})
}

exports.adminBoard = (req, res) => {
	User.findOne({
		where: {id: req.userId},
		attributes: ['name', 'username', 'email'],
		include: [{
			model: Role,
			attributes: ['id', 'name'],
			through: {
				attributes: ['userId', 'roleId'],
			}
		}]
	}).then(user => {
		res.status(200).json({
			"description": "Admin Board",
			"user": user
		});
	}).catch(err => {
		res.status(500).json({
			"description": "Can not access Admin Board",
			"error": err
		});
	})
}

exports.managementBoard = (req, res) => {
	User.findOne({
		where: {id: req.userId},
		attributes: ['name', 'username', 'email'],
		include: [{
			model: Role,
			attributes: ['id', 'name'],
			through: {
				attributes: ['userId', 'roleId'],
			}
		}]
	}).then(user => {
		res.status(200).json({
			"description": "Management Board",
			"user": user
		});
	}).catch(err => {
		res.status(500).json({
			"description": "Can not access Management Board",
			"error": err
		});
	})
}